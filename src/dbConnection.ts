import path from "path";
import { DataSource, DataSourceOptions } from "typeorm";

export const getConnection = (drop = true) => {
    const options: DataSourceOptions = {
        type: "postgres",
        host: "localhost",
        port: Number(process.env.POSTGRES_PORT) || 5432,
        username: process.env.POSTGRES_USER || "demo",
        password: process.env.POSTGRES_PASSWORD || "demo",
        entities: [path.join(__dirname, "./entities/*.{ts,js}")],
        synchronize: true,
        dropSchema: drop,
        logging: false,
    };
    const connection = new DataSource(options);
    return connection;
};