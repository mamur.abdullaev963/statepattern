import Express from "express";
import swaggerUi from "swagger-ui-express";
import { categoryRouter } from "./routes/categoryRouter";
import { productRouter } from "./routes/productRouter";
import { imageRouter } from "./routes/imageRouter";
import swaggerDocument from "./docs/swagger.json";

export const app = Express();
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(Express.json());
app.use(categoryRouter);
app.use(productRouter);
app.use(imageRouter);


