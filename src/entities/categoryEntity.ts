import { ArgsType, Field, InputType, ObjectType } from "type-graphql";
import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./productEntity";


@ObjectType()
@Entity()
export class Category extends BaseEntity {

  @Field()
  @PrimaryGeneratedColumn()
      id: number;

  @Field()
  @Column({ name: "parent_id", default: 0 })
      parentId: number;
  
  @Field()
  @Column({ name: "name", nullable: false })
      categoryName!: string;

  @OneToMany(()=> Product, product => product.category)
      products: Product[];
}

@InputType()
@ArgsType()
export class CategoryInputType {
  @Field({nullable: true, defaultValue: 0})
      parentId: number;
  
  @Field()
      categoryName: string;
}