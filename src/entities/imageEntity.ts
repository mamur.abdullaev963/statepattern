import { ArgsType, Field, InputType, ObjectType } from "type-graphql";
import { BaseEntity, Column, Entity, ManyToOne,PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./productEntity";

@InputType()
@ObjectType()
@Entity()
export class Image extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
      id: number;

  @Field()
  @Column({ name: "url", nullable: false})
      url: string;
  

  @ManyToOne(()=> Product, product => product.images)
      product: Product;
}

@ArgsType()
@InputType()
export class ImageInputType {
  @Field()
      data: string;
  
  @Field({nullable: true})
      thumbnail: boolean;
}