import { ArgsType, Field, ObjectType } from "type-graphql";
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { State } from "../controllers/productState";
import {Image, ImageInputType } from "./imageEntity";
import { Category } from "./categoryEntity";

@ObjectType()
@Entity()
export class Product extends BaseEntity {

  @Field()
  @PrimaryGeneratedColumn()
      id: number;

  @Field()
  @Column({ name: "name", nullable: false })
      productName: string;

  @Field()
  @Column({ name: "price", default: 0 })
      productPrice: number;
  
  @Field()
  @Column({ name: "thumnail_id", default:0})
      thumbnailId: number;

  @Field()
  @Column({ name: "state", default: State.DRAFT})
      state: State;

  @ManyToOne(()=> Category, category => category.products, {nullable: true})
      category: Category;

      
  @OneToMany(() => Image, image => image.product)
      images: Image[];

}

@ArgsType()
export class ProductArgsTypes {

  @Field({nullable: true})
      categoryId: number;
  
  @Field(()=> [ImageInputType])
      images: ImageInputType[];

  @Field()
      productName!: string;

  @Field()
      productPrice!: number;
}