import {Router} from "express";
import { ProductController } from "../controllers/productController";
import { Action, State } from "../controllers/productState";
import { customErrors } from "../utils";
export const productRouter = Router();

/** Product APIs */
productRouter.post("/product", async (req, res) => {
    const body = req.body;
    const productName = body.productName;
    const productPrice = body.productPrice;
    const images = body.images;
    const categoryId = body.categoryId;
    const productController = new ProductController();
    try{
        
        const product = await productController.saveProduct({categoryId, productName, productPrice, images});
        res.send(product);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});

productRouter.get("/products", async(_req, res) => {
    const productController = new ProductController();
    try{
        const products = await productController.getProducts();
        res.send(products);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }

});

productRouter.get("/product/:id", async(req, res) => {
    const productId = Number(req.params.id);
    const productController = new ProductController();
    if(productId <= 0){
        res.statusCode = 405;
        res.send("Invalid input parentId");
        return;
    }
    const product = await productController.getProductByProductId(productId);
    if(!product){
        res.statusCode = 404;
        res.send("Product not found");
        return;
    }
    res.send(product);
});

productRouter.get("/products/:state", async(req, res) => {
    const state = req.params.state as State;
    const productController = new ProductController();

    try{
        const products = await productController.getProductByState(state);
        res.send(products);
    }catch(e){
        
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }    
});

productRouter.post("/product/:id/:action", async (req, res) => {
    const productController = new ProductController();
    const action = req.params.action as Action;
    const productId = Number(req.params.id);
    if(productId <= 0){
        res.statusCode = 405;
        res.send("Invalid input parentId");
        return;
    }
    try{
        const product = await productController.updateProductState(productId, action);
        if(product){
            res.send(product);
        }else{
            res.statusCode = 404;
            res.send(`Product with id ${productId} not found`);
        }
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});