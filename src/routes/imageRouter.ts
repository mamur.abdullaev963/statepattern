import {Router} from "express";
import { ImageController } from "../controllers/imageController";
import { customErrors } from "../utils";
export const imageRouter = Router();

/** Image APIs */

imageRouter.get("/image/:id", async (req, res) => {
    const imageId = Number(req.params.id);
    const imageController = new ImageController();
    if(imageId <= 0){
        res.statusCode = 405;
        res.send(`Invalid input imageId: ${imageId}`);
        return;
    }
    try{

        const image = await imageController.getImage(imageId);
        if(!image){
            res.statusCode = 404;
            res.send("Image not found");
            return;
        }
        res.send(image);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});

imageRouter.get("/images/:productId", async (req, res) => {
    const productId = Number(req.params.productId);
    if(productId <= 0){
        res.statusCode = 405;
        res.send(`Invalid input productId: ${productId}`);
        return;
    }
    try{

        const imageController = new ImageController();
        const images = await imageController.getImagesByProductId(productId);
        if(images?.length == 0){
            res.statusCode = 404;
            res.send("Images not found");
            return;
        }
        res.send(images);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});



imageRouter.get("/image/download/:id", async (req, res) => {
    const imageId = Number(req.params.id);
    if(imageId <= 0){
        res.statusCode = 405;
        res.send(`Invalid input imageId: ${imageId}`);
        return;
    }
    
    try{
        const imageController = new ImageController();
        const imageData = await imageController.downloadImage(imageId);
        res.send({data: imageData});
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});

imageRouter.post("/images/:productId", async (req, res) => {
    const productId = Number(req.params.productId);
    const imageData = req.body.data;
    const thumbnail = req.body.thumbnail;

    if(productId <= 0){
        res.statusCode = 405;
        res.send(`Invalid input productId : ${productId}`);
        return;
    }

    try{
        const imageController = new ImageController();
        const image = await imageController.saveProductImage(productId, {data: imageData, thumbnail: thumbnail});
        res.send(image);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});