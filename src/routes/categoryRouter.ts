import {Router} from "express";
import { CategoryController } from "../controllers/catogoryController";
import { customErrors } from "../utils";
export const categoryRouter = Router();

/** Category APIs */
categoryRouter.get("/categories", async (_req, res) => {
    const categoryController = new CategoryController();
    try{
        const categories = await categoryController.getCategoryList();
        res.send(categories);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});

categoryRouter.get("/categories/:parentId", async (req, res) => {
    const categoryController = new CategoryController();
    const parentId = Number(req.params.parentId);
    if(parentId <= 0){
        res.statusCode = 405;
        res.send("Invalid input parentId");
        return;
    }
    try{

        const categories = await categoryController.getCategoryByParentId(parentId);
        if(categories.length == 0){
            res.statusCode = 404;
            res.send("Category not found");
            return;
        }
        res.send(categories);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});

categoryRouter.get("/category/:id", async (req, res) => {
    const categoryController = new CategoryController();
    const categoryId = Number(req.params.id);
    if(categoryId <= 0){
        res.statusCode = 405;
        res.send("Invalid input parentId");
        return;
    }
    try {
        const category = await categoryController.getCategoryById(categoryId);
        if(!category){
            res.statusCode = 404;
            res.send("Category not found");
            return;
        }
        res.send(category);
    }catch(e) {
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});

categoryRouter.post("/category", async (req, res) => {
    const body = req.body;
    const categoryName = body.categoryName;
    const parentId = body.parentId;
    if( parentId <= 0){
        res.statusCode = 405;
        res.send("Invalid input parentId");
        return;
    }

    if(!categoryName){
        res.statusCode = 405;
        res.send("categoryName is not provided");
        return;
    }
    const categoryController = new CategoryController();
    try{
        const category = await categoryController.saveCategory({categoryName, parentId});
        res.send(category);
    }catch(e){
        if(e instanceof Object){
            customErrors(e,res);
            return;
        }
        throw new Error(e);
    }
});