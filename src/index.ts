import "reflect-metadata";
import dotenv from "dotenv";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { DataSource } from "typeorm";
import { CategoryController } from "./controllers/catogoryController";
import { ProductController } from "./controllers/productController";
import { ImageController } from "./controllers/imageController";
import { app } from "./app";
import { getConnection } from "./dbConnection";
import { createStaticDirs } from "./utils";

dotenv.config();
export let connection: DataSource;
const main = async () => {

    if(process.env.ENV_MODE === "TEST"){
        connection = getConnection();
    }else{
        connection = getConnection(false);
    }
    await connection.initialize();
    
    createStaticDirs();
    
    const schema = await buildSchema({
        resolvers: [CategoryController, ProductController, ImageController]
    });
    const server = new ApolloServer({
        schema 
    });
    
    await server.start();
    server.applyMiddleware({app: app});

    app.listen(process.env.PORT || 8088, () => {
        console.log(`Server is running on http:/localhost:${process.env.PORT || 8088}${server.graphqlPath}`);
    });
};

main();