import { Arg, Args, Mutation, Query, Resolver } from "type-graphql";
import { connection } from "../index";
import { Category, CategoryInputType } from "../entities/categoryEntity";
import { generateCustomError } from "../utils";
import { ErrorKeys } from "../types";

@Resolver(Category)
export class CategoryController {
  
  @Query(() => [Category])
    async getCategoryList(): Promise<Category[]>{
        return new Promise( (resolve, reject) => {
           
                
            const categoryRepository = connection.getRepository(Category);
            categoryRepository.find().then((categories) => {
                resolve(categories);
            }).catch((e) => {
                reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));        
            });
            
        });
    }
  
  @Query(() => [Category])
  async getCategoryByParentId(@Arg("parentId", { nullable:false }) parentId: number): Promise<Category[]>{
      return new Promise((resolve, reject) => {
         
          const categoryRepository = connection.getRepository(Category);
          categoryRepository.findBy({parentId}).then((categories) => {
              resolve(categories);
          }).catch((e) => {
              reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
          });
      });
  }

  @Query(() => Category)
  async getCategoryById(@Arg("id", { nullable:false }) id: number): Promise<Category|null>{
      return new Promise((resolve, reject) => {
         
          if(!id){
              reject(generateCustomError(ErrorKeys.INVALID_INPUT, `id ${id} should be number or int`));
          }
          const categoryRepository = connection.getRepository(Category);
          categoryRepository.findOne({where: {id}}).then((category) => {
              resolve(category);
          }).catch((e) => {
              reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
          });
    
      });}



  @Mutation(() => Category)
  async saveCategory(
    @Args(){ categoryName, parentId }: CategoryInputType
  ): Promise<Category|undefined>{
      return new Promise( (resolve, reject) => {
          const category = new Category();
          if(parentId <= 0){
              reject(generateCustomError(ErrorKeys.INVALID_INPUT, `invalid input parentId: ${parentId}`));
          }
          category.categoryName = categoryName;
          
          if(!parentId){
              category.save().then((data) => {
                  resolve(data);
              }).catch((e) => {
                  reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
              });
          }else{

              this.getCategoryById(parentId).then((parentCategory) => {
                  if(parentCategory){
                      category.parentId = parentCategory.id;
                  }else{
                      reject(generateCustomError(ErrorKeys.NOT_FOUND, `parent category with id ${parentId} not found`));
                      return;
                  }
                  
                  category.save().then((data) => {
                      resolve(data);
                  }).catch((e) => {
                      reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
                  });
    
              }).catch((e) => {
                  reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
              });
          }

          
          
       
          
      });
  }
}
