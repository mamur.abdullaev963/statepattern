
interface IProductState {
  product: ProductState;

  listDraft():void;
  exceedDuration():void;
  renewListing():void;
  delete():void;
  proceedPayment():void;
  paymentSuccess():void;
  disputeAccept():void;
  rePublish():void;
  failPayment():void;
  getName():State;
}
export enum State {
  AVAILABLE = "AVAILABLE",
  DELETED = "DELETED",
  SOLD = "SOLD",
  RESERVED = "RESERVED",
  RETURNED = "RETURNED",
  EXPIRED = "EXPIRED",
  DRAFT = "DRAFT"
}

export enum Action {
  LIST_DRAFT="LIST_DRAFT",
  EXCEED_DURATION="EXCEED_DURATION",
  RENEW_LISTING="RENEW_LISTING",
  DELETE="DELETE",
  PROCEED_PAYMENT="PROCEED_PAYMENT",
  PAYMENT_SUCCESS="PAYMENT_SUCCESS",
  RE_PUBLISH="RE_PUBLISH",
  FAIL_PAYMENT="FAIL_PAYMENT",
}

export class ProductState {

    public currentState: IProductState;

    public draftState: IProductState;
    public availableState: IProductState;
    public expiredState: IProductState;
    public deletedState: IProductState;
    public reservedState: IProductState;
    public soldState: IProductState;
    public returnedState: IProductState;
  
    constructor(state: State){
        this.draftState = new DraftState(this);
        this.availableState = new AvailableState(this);
        this.expiredState = new ExpiredState(this);
        this.deletedState = new DeletedState(this);
        this.reservedState = new ReservedState(this);
        this.soldState = new SoldState(this);
        this.returnedState = new ReturnedState(this);

        switch(state){
        case State.AVAILABLE: {
            this.setState(this.availableState);
            break;
        }
        case State.DELETED: {
            this.setState(this.deletedState);
            break;
        }
        case State.DRAFT: {
            this.setState(this.draftState);
            break;
        }
        case State.EXPIRED: {
            this.setState(this.expiredState);
            break;
        }
        case State.RESERVED: {
            this.setState(this.reservedState);
            break;
        }
        case State.RETURNED: {
            this.setState(this.returnedState);
            break;
        }
        case State.SOLD: {
            this.setState(this.soldState);
            break;
        }
        default:{
            throw new Error("Not valid state");
        }
        }
    }


    async setState(state: IProductState){
        this.currentState = state;
    }

    public getState(){
        return this.currentState;
    }
}

class DraftState implements IProductState {
    product: ProductState;

    constructor(product: ProductState){
        this.product = product;
    }
    getName(): State {
        return State.DRAFT;
    }
    failPayment(): void {
        throw new Error("Only possible for RESERVED products");
    }
    listDraft(): void {
        this.product.setState(this.product.availableState);
    }
    exceedDuration(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    renewListing(): void {
        throw new Error("Only possible for EXPIRED");
    }
    delete(): void {
        this.product.setState(this.product.deletedState);
    }
    proceedPayment(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    paymentSuccess(): void {
        throw new Error("Only possible for RESERVED products");
    }
    disputeAccept(): void {
        throw new Error("Only possible for SOLD products");
    }
    rePublish(): void {
        throw new Error("Only possible for RETURNED producs");
    }
}
class AvailableState implements IProductState {
    product: ProductState;

    constructor(product: ProductState){
        this.product = product;
    }
    getName(): State {
        return State.AVAILABLE;
    }
    failPayment(): void {
        throw new Error("Only possible for RESERVED products");
    }
    listDraft(): void {
        throw new Error("Only possible for DRAFT products");
    }
    exceedDuration(): void {
        this.product.setState(this.product.expiredState);
    }
    renewListing(): void {
        throw new Error("Only possible for EXPIRED products");
    }
    delete(): void {
        this.product.setState(this.product.deletedState);
    }
    proceedPayment(): void {
        this.product.setState(this.product.reservedState);
    }
    paymentSuccess(): void {
        throw new Error("Only possible for RESERVED products");
    }
    disputeAccept(): void {
        throw new Error("Only possible for SOLD products");
    }
    rePublish(): void {
        throw new Error("Only possible for RETURNED producs");
    }
}
class ExpiredState implements IProductState {
    product: ProductState;

    constructor(product: ProductState){
        this.product = product;
    }
    getName(): State {
        return State.EXPIRED;
    }
    failPayment(): void {
        throw new Error("Only possible for RESERVED products");
    }
    listDraft(): void {
        throw new Error("Only possible for DRAFT products");
    }
    deleteDraft(): void {
        throw new Error("Method not implemented.");
    }
    exceedDuration(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    renewListing(): void {
        this.product.setState(this.product.availableState);
    }
    delete(): void {
        throw new Error("Only possible for DRAFT, AVAILABLE or RETERNET products");
    }
    proceedPayment(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    paymentSuccess(): void {
        throw new Error("Only possible for RESERVED products");
    }
    disputeAccept(): void {
        throw new Error("Only possible for SOLD products");
    }
    rePublish(): void {
        throw new Error("Only possible for RETURNED producs");
    }
}
class DeletedState implements IProductState {
    product: ProductState;

    constructor(product: ProductState){
        this.product = product;
    }
    getName(): State {
        return State.DELETED;
    }
    failPayment(): void {
        throw new Error("Only possible for RESERVED products");
    }
    listDraft(): void {
        throw new Error("Only possible for DRAFT products");
    }
    deleteDraft(): void {
        throw new Error("Method not implemented.");
    }
    exceedDuration(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    renewListing(): void {
        throw new Error("Only possible for EXPIRED");
    }
    delete(): void {
        throw new Error("Only possible for DRAFT, AVAILABLE or RETERNET products");
    }
    proceedPayment(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    paymentSuccess(): void {
        throw new Error("Only possible for RESERVED products");
    }
    disputeAccept(): void {
        throw new Error("Only possible for SOLD products");
    }
    rePublish(): void {
        throw new Error("Only possible for RETURNED producs");
    }
}
class ReservedState implements IProductState {
    product: ProductState;

    constructor(product: ProductState){
        this.product = product;
    }
    getName(): State {
        return State.RESERVED;
    }
    failPayment(): void {
        this.product.setState(this.product.availableState);
    }
    listDraft(): void {
        throw new Error("Only possible for DRAFT products");
    }
    deleteDraft(): void {
        throw new Error("Method not implemented.");
    }
    exceedDuration(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    renewListing(): void {
        throw new Error("Only possible for EXPIRED");
    }
    delete(): void {
        throw new Error("Only possible for DRAFT, AVAILABLE or RETERNET products");
    }
    proceedPayment(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    paymentSuccess(): void {
        this.product.setState(this.product.soldState);
    }
    disputeAccept(): void {
        throw new Error("Only possible for SOLD products");
    }
    rePublish(): void {
        throw new Error("Only possible for RETURNED producs");
    }
}
class SoldState implements IProductState {
    product: ProductState;

    constructor(product: ProductState){
        this.product = product;
    }
    getName(): State {
        return State.SOLD;
    }
    failPayment(): void {
        throw new Error("Only possible for RESERVED products");
    }
    listDraft(): void {
        throw new Error("Only possible for DRAFT products");
    }
    deleteDraft(): void {
        throw new Error("Method not implemented.");
    }
    exceedDuration(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    renewListing(): void {
        throw new Error("Only possible for EXPIRED");
    }
    delete(): void {
        throw new Error("Only possible for DRAFT, AVAILABLE or RETERNET products");
    }
    proceedPayment(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    paymentSuccess(): void {
        throw new Error("Only possible for RESERVED products");
    }
    disputeAccept(): void {
        this.product.setState(this.product.returnedState);
    }
    rePublish(): void {
        throw new Error("Only possible for RETURNED producs");
    }
}
class ReturnedState implements IProductState {
    product: ProductState;

    constructor(product: ProductState){
        this.product = product;
    }
    getName(): State {
        return State.RETURNED;
    }
    failPayment(): void {
        throw new Error("Only possible for RESERVED products");
    }
    listDraft(): void {
        throw new Error("Only possible for DRAFT products");
    }
    deleteDraft(): void {
        throw new Error("Method not implemented.");
    }
    exceedDuration(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    renewListing(): void {
        throw new Error("Only possible for EXPIRED");
    }
    delete(): void {
        this.product.setState(this.product.deletedState);
    }
    proceedPayment(): void {
        throw new Error("Only possible for AVAILABLE products");
    }
    paymentSuccess(): void {
        throw new Error("Only possible for RESERVED products");
    }
    disputeAccept(): void {
        throw new Error("Only possible for SOLD products");
    }
    rePublish(): void {
        this.product.setState(this.product.draftState);
    }
}