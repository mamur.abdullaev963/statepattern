
import { Arg, Args, Mutation, Query, Resolver } from "type-graphql";
import { connection } from "../index";
import { Product, ProductArgsTypes } from "../entities/productEntity";
import { Category } from "../entities/categoryEntity";
import { ImageController } from "./imageController";
import { ProductState, State, Action } from "./productState";
import { generateCustomError } from "../utils";
import { ErrorKeys } from "../types";

@Resolver(Product)
export class ProductController {

  @Query(() => [Product])
    async getProductByState(@Arg("state") state: State): Promise<Product[]>{
        return new Promise((resolve, reject) => {
            
            if (!Object.values(State).includes(state)) {
                reject(generateCustomError(ErrorKeys.INVALID_INPUT, `state ${state} is not typeof State`));
                return;
            }
            const productRepository = connection.getRepository(Product);
            productRepository.findBy({state}).then((products) => {
                resolve(products);
            }).catch((e) => {
                reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));

            });
          
        });
    }

  @Query(() => Product)
  async getProductByProductId(@Arg("id") id: number): Promise<Product|null>{
      return new Promise((resolve, reject) => {
         
          const productRepository = connection.getRepository(Product);
          productRepository.findOne({where: {id}}).then((product) => {
              resolve(product);
          }).catch((e) => {
              reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
          });
          
      });
  }

  @Query(() => [Product])
  async getProducts():Promise<Product[]>{
      return new Promise((resolve, reject) => {
         
          const productRepository = connection.getRepository(Product);
          productRepository.find().then((products) => {
              resolve(products);
          }).catch((e) => {
              reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
          });

      });
  }

  @Mutation(() => Product)
  async saveProduct(
    @Args(){ categoryId, productName, productPrice, images }: ProductArgsTypes
  ): Promise<Product|undefined|Error>{

      const product = new Product();
      const categoryRepository = await connection.getRepository(Category);
      const imageController = new ImageController();
      //get product category
      if(categoryId){
          try{

              const category = await categoryRepository.findOne({where: {id: categoryId}});
              if(category){
                  product.category = category;
              }
          }catch(e){
              return Promise.reject(e);
          }
      }
      product.productName = productName;
      product.productPrice = productPrice;
      const newProduct = await product.save();
      if(images){
          
          for(const image of images){
              try{
                  const newImage = await imageController.saveProductImage(newProduct.id, {data: image.data, thumbnail: image.thumbnail});

                  if(image.thumbnail){
                      newProduct.thumbnailId = newImage.id;
                      await newProduct.save();
                  }
              }catch(e){
                  return Promise.reject(e);
              }
          }
          
      }
      return Promise.resolve(newProduct);
    
  }
        
  

  @Mutation(() => Product)
  async updateProductState(@Arg("id") id: number, @Arg("action") action: Action): Promise<Product>{

      return new Promise((resolve, reject) => {
          if (!Object.values(Action).includes(action)) {
              reject(generateCustomError(ErrorKeys.INVALID_INPUT, `state ${action} is not typeof Action`));
              return;
          }
          this.getProductByProductId(id).then((product) =>{
              if(product){
                  try{
                      const productState = new ProductState(product.state as State);
                      switch(action){
                      case Action.DELETE: {
                          productState.getState().delete(); break;
                      }
                      case Action.EXCEED_DURATION: {
                          productState.getState().exceedDuration(); break;
                      }
                      case Action.FAIL_PAYMENT: {
                          productState.getState().failPayment(); break;
                      }
                      case Action.LIST_DRAFT: {
                          productState.getState().listDraft(); break;
                      }
                      case Action.PAYMENT_SUCCESS: {
                          productState.getState().paymentSuccess(); break;
                      }
                      case Action.PROCEED_PAYMENT: {
                          productState.getState().proceedPayment(); break;
                      }
                      case Action.RENEW_LISTING: {
                          productState.getState().renewListing(); break;
                      }
                      case Action.RE_PUBLISH: {
                          productState.getState().rePublish(); break;
                      }
                      default:{
                          reject(generateCustomError(ErrorKeys.INVALID_INPUT));
                          return;
                      }
                      }
                      // must change productState 
                      product.state = productState.getState().getName();
                      product.save().then((savedProduct)=> {
                          resolve(savedProduct);
                      }).catch((e) => {
                          reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
                          return;
                      });

                  }catch(e) {
                      reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
                      return;
                  }
              }
              else{
                  reject(generateCustomError(ErrorKeys.NOT_FOUND, `Product with id ${id} not found`));
                  return;
              }
          }).catch((e) =>{
              reject(generateCustomError(e));
              return;
          });
         
              
         
              
          
      });
  }
}
