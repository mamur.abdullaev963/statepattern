import fs from "fs";
import path from "path";
import { v4 as uuid } from "uuid";
import { Image, ImageInputType } from "../entities/imageEntity";
import { ProductController } from "./productController";
import { Query, Resolver, Arg, Mutation, Args } from "type-graphql";
import { connection } from "../index";
import { decodeBase64Image, generateCustomError } from "../utils";
import { ErrorKeys } from "../types";

@Resolver(Image)
export class ImageController {

  @Query(() => Image)
    async getImage(@Arg("id", {nullable: false}) id: number ):Promise<Image|null> {
        return new Promise((resolve, reject) => {
         
            const imageRepository = connection.getRepository(Image);
            imageRepository.findOne({where: {id}}).then((image) => {
                resolve(image);
            }).catch((e) => {
                reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
            });
           
           
        });
    }

  @Query(() => [Image])
  async getImagesByProductId(@Arg("productId", {nullable: false}) productId: number): Promise<Image[]|undefined>{
      return new Promise((resolve, reject) => {
          
          const imageRepository = connection.getRepository(Image);
          imageRepository
              .find({where:{product: {id: productId}}, relations: ["product"]})
              .then((images) => {
                  resolve(images);
              }).catch((e) => {
                  reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
              });
      });
  }

  @Query(() => String)
  async downloadImage(@Arg("id", {nullable: false }) id: number): Promise<string|null>{
      return new Promise((resolve, reject) => {
          this.getImage(id).then((image) => {
              if(!image){
                  reject(generateCustomError(ErrorKeys.NOT_FOUND, `Image not found with id ${id}`));
              }
              fs.readFile(path.join(process.cwd(), image?.url || "" ), "base64", (err,data) => {
                  if(err){
                      reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, "cannot read image file from source"));
                      return;
                  }
                  resolve(data);
                  return;
              });
          }).catch((error) => {
              reject(error);
              return;
          });

      });
  }

  @Mutation(() => Image)
  async saveProductImage(
    @Arg("productId", {nullable: false}) productId: number,
    @Args() { data, thumbnail }: ImageInputType
  ):Promise<Image>{
      return new Promise((resolve, reject) => {
          try{
              const productController = new ProductController();
              productController.getProductByProductId(productId).then((product) =>{
                  if(!product){
                      reject(generateCustomError(ErrorKeys.NOT_FOUND, `product with id ${productId} not found`));
                      return; 
                  }
                  if(data){
                      const dataString = decodeBase64Image(data);
                      if(dataString.length > 0){

                          const imageName = uuid()+".png";
                          const url = path.join(process.env.STATIC_DIR || "src/static/", "images", imageName);
          
          
                          fs.writeFile(path.join(process.cwd(),url), dataString, "base64", function(err: any) {
                              if(err){
                                  console.log(err);
                              }
                          });

                          const newImage = new Image();
                          newImage.url = url;
                          newImage.product = product;
                          newImage.save().then((newSavedImage) => {
                              if(thumbnail){
                                  product.thumbnailId = newSavedImage.id;       
                              }
                              product.save().then(() => {
                                  resolve(newImage);
                              });

                          });

                      }else{
                          reject(generateCustomError(ErrorKeys.INVALID_INPUT, "invalid input format"));
                      }
                  }else{
                      reject(generateCustomError(ErrorKeys.INVALID_INPUT, "image data cannot be null"));
                  }
              }).catch((error) =>{
                  reject(error);
              });
            

          }catch(e){
              reject(generateCustomError(ErrorKeys.INTERNAL_SERVER_ERROR, e.message));
          }
      });

  }
}