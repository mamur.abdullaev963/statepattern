import fs from "fs";
import path from "path";
import { Response } from "express";
import { ErrorKeys } from "./types";

export function decodeBase64Image(data: string): string {
    const matches = data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

    if (!matches || matches.length !== 3) {
        return "";
    }

    return matches[2];
}


export function generateCustomError(key: string, message?: string){
    const obj = {
        [key]: message
    };
    return obj;
}

export function customErrors(error:any, res:Response) {
    const errorKey = Object.keys(error)[0];
    const errorMessage = error[errorKey];
    if(errorKey === ErrorKeys.INTERNAL_SERVER_ERROR){
        res.statusCode=500;
    }else if(errorKey === ErrorKeys.MISSING_DATA || errorKey === ErrorKeys.INVALID_INPUT){
        res.statusCode = 405;
    }else if(errorKey === ErrorKeys.NOT_FOUND){
        res.statusCode = 404;
    }

    res.statusMessage = errorMessage;
    res.send();
}

export function createStaticDirs(){
    const static_dir = process.env.STATIC_DIR || "src/static/";
    if (!fs.existsSync(static_dir)){
        fs.mkdirSync(static_dir);
    }

    const image_dir = path.join(process.env.STATIC_DIR || "src/static/", "images");
    console.log("came here", image_dir);
    if (!fs.existsSync(image_dir)){
        fs.mkdirSync(image_dir, {recursive:true});
    }
}