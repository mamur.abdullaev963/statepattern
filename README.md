**Description:**

This is the demo project to demonstrate combination of REST and GRAPHQL API.

Available APIs:
1. POST(save) `category`, `product` and `images`
2. GET `category`, `product` and `images`



## How to set up locally

1. clone the repository and go to project directory
2. Crate `.env` file project root directory and copy past `.env.example`. Update if necessary.
3. Project uses Postgres to save and read data. To install Posgress in dokcer(recommended) run 
  `docker run -e POSTGRES_PASSWORD=demo -e POSTGRES_USER=demo -p 5432:5432 -d postgres`
  or manually install to OS.
  If you change `password`, `user` or `port` for Postgres make sure to update `.env` file with new values.
4. `npm i` to install required packages
5. `npm run test` to make sure all APIs work
6. `npm run dev` usefull while in development
7. `npm run build` builds the project
8. `npm start` runs built project

## How to test APIs

1. To test REST api go to `http://localhost:${process.env.port}/docs`
2. To test GRAPHQL api go to `http://localhost:8088/graphql` it will redirect you to Apollo Studio 