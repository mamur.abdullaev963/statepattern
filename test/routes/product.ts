import  {SuperAgentTest}  from "supertest"; 

export const product = (agent: SuperAgentTest) => {

    describe("product", () => {
        describe("POST /proudct", () => {
            it("it should save without categoryId and images data and return 200", async () => {
                const response = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 12,
                    }
                );

                expect(response.body.state).toMatch("DRAFT");
                expect(response.body.productName).toMatch("productName");
                expect(response.body.productPrice).toEqual(12);

            });

            it("it should save with categoryId and images data and return 200", async () => {
                const category = await agent.post("/category").send({categoryName:"categoryName",parentId:1});
                const categoryId = category.body.id;
                const response = await agent.post("/product").send(
                    {
                        productName: "productName5",
                        productPrice: 15,
                        categoryId: categoryId,
                        images: [
                            {
                                data:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QB6RXhpZgAASUkqAA",
                                thumbnail: false
                            }
                        ]
                    }
                );
                
                expect(response.body.state).toMatch("DRAFT");
                expect(response.body.productName).toMatch("productName5");
                expect(response.body.productPrice).toEqual(15);
                expect(response.body.category.id).toEqual(categoryId);

            });
        });

        describe("GET /products", () => {
            it("should return 200 with list of products", async () => {
                const products = await agent.get("/products");
                expect(products.statusCode).toBe(200);
                expect(products.body).toBeInstanceOf(Object);
            });
        });

        describe("GET /product/:id", () => {
            it("should return 405 if id is not valid", async () => {
                const response = await agent.get("/product/0");
                await expect(response.statusCode).toBe(405);
            });

            it("should return 404 when there is no product for id", async () => {
                const response = await agent.get("/product/654987");
                await expect(response.statusCode).toBe(404);
            });

            it("should return 200 when no error", async () => {
                const newProduct = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 10,
                    });
            
                const productId = newProduct.body.id;

                const response = await agent.get("/product/"+productId);
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toEqual(productId);
            });
        });

        describe("GET /products/:state", () => {
            it("should return 405 with invalid state", async () => {
                const response = await agent.get("/products/NON_EXISTING_STATE");
                expect(response.statusCode).toBe(405);
            });

            it("should return 200 with empty list of products when there is no error", async () => {
                const response = await agent.get("/products/DRAFT");
                expect(response.statusCode).toBe(200);
                expect(response.body).toBeInstanceOf(Object);
            });
        });

        describe("POST /product/:id/:action", () => {
            
            it("should return 405 if id is not valid", async () => {
                const response = await agent.post("/product/0/action");
                await expect(response.statusCode).toBe(405);
            });

            it("should return 404 when there is no product for id", async () => {
                const response = await agent.post("/product/654987/LIST_DRAFT");
                await expect(response.statusCode).toBe(404);
            });

            it("should return 405 when action is not valid", async () => {
                const response = await agent.post("/product/654987/DRAFT");
                await expect(response.statusCode).toBe(405);
            });

            it("should return 500 when product.state is DRAFT and action is PROCEED_PAYMENT", async () => {
                const newProduct = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 12,
                    }
                );
                const newProductId = newProduct.body.id;
                const response = await agent.post("/product/"+newProductId+"/PROCEED_PAYMENT");
                expect(response.statusCode).toBe(500);
            });

            it("should return 200 when product.state is DRAFT and action is LIST_DRAFT", async () => {
                const newProduct = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 12,
                    }
                );
                const newProductId = newProduct.body.id;
                const response = await agent.post("/product/"+newProductId+"/LIST_DRAFT");
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toEqual(newProductId);
            });
            
        });
    });
};