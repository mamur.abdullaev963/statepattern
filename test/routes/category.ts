import  {SuperAgentTest}  from "supertest"; 

export const category = (agent: SuperAgentTest) => {

    describe("category", () => {
       
        describe("GET /categories", () => {
  
            it("should return 200 with empty array", async ()=> {
                const response = await agent.get("/categories");
                expect(response.statusCode).toBe(200);
                expect(response.body).toMatchObject([]);
            });

            it("should return 200 with list of categories", async ()=> {

                await agent.post("/category").send({categoryName:"categoryName"});

                const response = await agent.get("/categories");
                expect(response.statusCode).toBe(200);
                expect(response.body[0]).toMatchObject({categoryName:"categoryName"});
            });
        });
  
        describe("GET /categories/:parentId", () => {
      
            it("should return 405 error when parentId is not valid", async () => {
                const response = await agent.get("/categories/-1");
                expect(response.statusCode).toBe(405);
            });
  
            it("should return 404 when there is no subCategories for parentId", async() => {
                const response = await agent.get("/categories/654231645");
                expect(response.statusCode).toBe(404);
            });
  
            it("should return 200 when no error", async () => {
                // mock function
                const newCategory = await agent.post("/category").send({categoryName: "newCategory"});
                const id = newCategory.body.id;
                await agent.post("/category").send({categoryName:"categoryName",parentId:id});
                const response = await agent.get("/categories/"+id);
                expect(response.statusCode).toBe(200);
                expect(response.body[0].categoryName).toMatch("categoryName");
                expect(response.body[0].parentId).toEqual(id);
            });
  
  
        });
  
        describe("GET /category/:id", () => {
      
            it("should return 405 if id is not valid", async () => {
                const response = await agent.get("/category/0");
                await expect(response.statusCode).toBe(405);
            });
  
            it("should return 404 when there is no category for id", async () => {
                const response = await agent.get("/category/654987");
                await expect(response.statusCode).toBe(404);
            });
  
  
            it("should return 200 when if no error", async () => {
                const newCategory = await agent.post("/category").send({categoryName:"singleCategoryName",parentId:1});
                const id = newCategory.body.id;
                const response = await agent.get("/category/"+id);
                expect(response.statusCode).toBe(200);
                expect(response.body.categoryName).toMatch("singleCategoryName");
                expect(response.body.id).toEqual(id);
            });
        });
  
        describe("POST /category", () => {
      
            it("should return 405 if parentId is not valid", async () => {
                const response = await agent.post("/category").send({"parentId":-1});
                expect(response.statusCode).toBe(405);
            });
  
            it("should return 405 if categoryName is not provided", async () => {
                const response = await agent.post("/category").send({"parentId":1});
                expect(response.statusCode).toBe(405);
            });
  
            it("should return 200 when parentId is valid", async () => {
                const response = await agent.post("/category").send({categoryName:"categoryName",parentId:1});
                expect(response.statusCode).toBe(200);
  
            });
        });
    });
};

