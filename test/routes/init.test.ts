import request from "supertest";
import { category } from "./category";
import { image } from "./image";
import { product } from "./product";
import { globalSetup, globalTeardown} from "../config";

const agent = request.agent("http://localhost:8088");


jest.setTimeout(40*1000);
beforeAll(async () => {
    await globalSetup();
});

afterAll(async () => {
    await globalTeardown();
});

describe("route", () => {
    category(agent);
    image(agent);
    product(agent);
});