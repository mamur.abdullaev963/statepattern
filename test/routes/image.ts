
import  {SuperAgentTest}  from "supertest"; 

export const image = (agent: SuperAgentTest) => {
    describe("image", () => {
        describe("GET /image/:id", () => {
        
            it("should return 405 if id is not valid", async () => {
                const response = await agent.get("/image/0");
                await expect(response.statusCode).toBe(405);
            });

            it("should return 404 when there is no image for id", async () => {
                const response = await agent.get("/image/654987");
                await expect(response.statusCode).toBe(404);
            });

            it("should return 200 when no error", async () => {
                // mock function

                const newProduct = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 10,
                    });
                
                const productId = newProduct.body.id;
                const newImage = await agent.post("/images/"+productId)
                    .send(
                        {
                            data:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QB6RXhpZgAASUkqAA",
                            thumbnail: false
                        }
                    );
                
                const newImageId = newImage.body.id;
                const response = await agent.get("/image/"+newImageId);
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toEqual(newImageId);
            });

        });

        describe("GET /images/:productId", () => {
            it("should return 405 error when productId is not valid", async () => {
                const response = await agent.get("/images/-1");
                expect(response.statusCode).toBe(405);
            });

            it("should return 404 when there is no images for productId", async() => {
                const response = await agent.get("/images/654231645");
                expect(response.statusCode).toBe(404);
            });

           

            it("should return 200 when no error", async () => {
            
                const newProduct = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 10,
                        images: [
                            {
                                data:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QB6RXhpZgAASUkqAA",
                                thumbnail: false
                            }
                        ]
                    });
                
                const productId = newProduct.body.id;
                const response = await agent.get("/images/"+productId);
                expect(response.statusCode).toBe(200);
                expect(response.body).toBeInstanceOf(Object);
            });

        });

        describe("GET /image/download/:id", () => {
            it("should return 405 error when id is not valid", async () => {
                const response = await agent.get("/image/download/-1");
                expect(response.statusCode).toBe(405);
            });

            it("should return 404 when there is no image", async() => {
                const response = await agent.get("/image/download/654231645");
                expect(response.statusCode).toBe(404);
            });

            it("should return 200 with downloaded image data", async () => {
                const newProduct = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 10,
                    });
                
                const productId = newProduct.body.id;
                const newImage = await agent.post("/images/"+productId)
                    .send(
                        {
                            data:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QB6RXhpZgAASUkqAA",
                            thumbnail: false
                        }
                    );
                const newImageId = newImage.body.id;
                const response = await agent.get("/image/download/"+newImageId);
                expect(response.statusCode).toBe(200);
                expect(response.body).toHaveProperty("data");
            });

        });

        describe("POST /images/:productId", () => {
            it("should return 405 error when productId is not valid", async () => {
                const response = await agent.post("/images/-1").send({data:"image data", thumbnail:false});
                expect(response.statusCode).toBe(405);
            });

            it("should return 404 when there is no product with productId", async() => {
                const response = await agent.post("/images/1111").send({data:"image data", thumbnail:false});
                expect(response.statusCode).toBe(404);
            });

            it("should save product image and return 200 if there is no error", async () => {
                const newProduct = await agent.post("/product").send(
                    {
                        productName: "productName",
                        productPrice: 10,
                    });
                
                const productId = newProduct.body.id;
              
                const response = await agent.post("/images/"+productId)
                    .send(
                        {
                            data:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QB6RXhpZgAASUkqAA",
                            thumbnail: false
                        }
                    );
                expect(response.statusCode).toBe(200);
                expect(response.body).toHaveProperty("url");
                expect(response.body).toHaveProperty("product");
            });
        });
    });
};