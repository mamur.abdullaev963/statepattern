import { PORT } from ".";
import { setup }  from "jest-dev-server";

export async function globalSetup() {

    await setup({
        command: "ENV_MODE=TEST npm run dev",
        launchTimeout: 20000,
        debug: true,
        port: PORT
    });
    console.log("server started");
}